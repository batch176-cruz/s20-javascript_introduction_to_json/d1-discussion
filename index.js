console.log("Hello!")

/*
	JSON Objects
		JSOn - JavaScript Object Notation
		JS Objects are not the same as JSON
		It uses double quotes for the property names

		Syntax:
			{
				"propertyA": "valueA",
				"propertyB": "valueB"
			}

*/

// JSON Objects
/*{
	"city": "Quezon City"
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Array
/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]*/

// JSON Method
// The JSON Object contains methods for parsing and converting data into a stringinfied JSON

// Converting data into stringified JSON

let batches = [
	{
		batchName: "Batch 176"
	},
	{
		batchName: "Batch 177"
	}
]

console.log(batches);

console.log(JSON.stringify(batches));

let data = JSON.stringify({
	name: 'John',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

/*
	Syntax:
		JSON.stringify({
			propertyA: valueA,
			propertyB: valueB
		})
*/

/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);*/

// Before sending data, convert an array or an object to its string equivalent
	// stringify()

let batches2 = `[
	{
		"batchName": "176"
	},
	{
		"batchName": "177"
	}
]`;

console.log(batches2);
console.log(JSON.parse(batches2));

let stringifiedObject = `{
	"name": "John",
	"age": "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`;

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));

